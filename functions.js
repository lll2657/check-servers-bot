function XHR(file, callback){
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(){
        if(xhr.readyState === 4 && xhr.status === 200){
            callback(xhr.responseText);
        }
    }
    xhr.open('GET', file, true);
    xhr.send();
}

$(document).ready(function(){
  XHR("results.json",function(e){
    var array = JSON.parse(e);
    var str = "<ul>";
    for(var i=0;i<array.length;i++){
      str += "<li><span>" + array[i].Name + ": </span>";
      if(array[i].Status==1){
        str += "<span class='greenCircle'></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
      }
      else{
        str += "<span class='redCircle'></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
      }
      str+="<span>Time: " + array[i].Time + "</span></li>";
    }
    str += "</ul>";
    list.innerHTML += str;
  });
});
