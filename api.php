<?php

include 'variables.php';

$data = json_decode(file_get_contents('list.json'));

$file = [];

foreach($data->$servers as $value){
	if($value->$post_or_get == "Get"){
		$result = json_decode(getRequest($value->$url));
	}
	else if($value->$post_or_get == "Post"){
		foreach($value->$header as $val){
			$headers .= $val . "\r\n";
		}
		$result = json_decode(postRequest($value->$url,$headers,$value->$body));
	}
	if(conditionValidation($result->$success)){
			$file[] = ['Name' => $value->$name, 'Status' => 1, 'Time' => date("h:i:sa")];
	}
	else{
		$file[] = ['Name' => $value->$name, 'Status' => 0, 'Time' => date("h:i:sa")];
		$msg = "The Server ".$value->$name." has returned an incorrect value";
		sendSMS($countryCode, $number, $msg);
		sendMail($to, "Server Problems",$msg);		
	}
}

$file = json_encode($file);
file_put_contents('results.json', print_r($file, true));

function conditionValidation($conditions)
{
	if($conditions == true){
		return true;
	}
	else{
		return false;
	}
}

function getRequest($url)
{
	return file_get_contents($url);
}

function postRequest($url, $header, $params)
{
	$options = array(
	    'http' => array(
	        'header'  => $header,
	        'method'  => 'POST',
	        'content' => json_encode($params)
	    )
	);
	$context  = stream_context_create($options);
	return file_get_contents($url, false, $context);
}

function sendSMS($countryCode, $number, $content)
{
	$phone_url = "http://api.accessyou.com/sms/sendsms-utf8.php?accountno=11029450&pwd=98863549&msg=".urlencode($content)."&phone=".$countryCode.$number;
	fopen($phone_url,'r');
	return;
}

function sendMail($to, $title, $content)
{
	mail($to, $title, $content);
	return;
}

?>